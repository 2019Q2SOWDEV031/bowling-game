package com.kata.tdd.game.bowling;

import java.util.ArrayList;
import java.util.List;

public class BowlingGame {
    private Frame frame;
    private List<Frame> frames;

    public BowlingGame() {
        frames = new ArrayList<>();
        frame = new Frame();
        frames.add(frame);
    }

    public String getTotalScore() {
        int totalScore = 0;
        for (int frameIndex = 0; frameIndex < frames.size() && frameIndex < 10; frameIndex++) {
            Frame currentFrame = frames.get(frameIndex);
            totalScore += currentFrame.getScore();
            totalScore += getSpareBonus(frameIndex, currentFrame);
            totalScore += getStrikeBonus(frameIndex, currentFrame);
        }
        return String.valueOf(totalScore);
    }

    private int getStrikeBonus(int frameIndex, Frame currentFrame) {
        int strikeBonus = 0;
        if(currentFrame.isStrike()) {
            Frame nextFrame = frames.get(frameIndex + 1);
            strikeBonus += nextFrame.getScore();

            if(nextFrame.isStrike()) {
                strikeBonus += frames.get(frameIndex + 2).getFirstRoll();
            }
        }
        return strikeBonus;
    }

    private int getSpareBonus(int currentFrameIndex, Frame currentFrame) {
        int spareBonus = 0;
        if (currentFrame.isSpare()) {
            spareBonus = frames.get(currentFrameIndex + 1).getFirstRoll();
        }
        return spareBonus;
    }

    public void addRoll(String roll) {
        if(frame.isFull()) {
            frame = new Frame();
            frames.add(frame);
        }
        frame.addRoll(roll);
    }
}
