package com.kata.tdd.game.bowling;

class Frame {
    private int[] rolls;
    private int rollIndex;

    Frame() {
        rolls = new int[2];
        rollIndex = 0;
    }

    int getScore() {
        return rolls[0] + rolls[1];
    }

    void addRoll(String roll) {
        if ("/".equals(roll)) {
            this.rolls[rollIndex] = 10 - rolls[0];
        } else if ("X".equals(roll)) {
            this.rolls[rollIndex] = 10;
        } else if ("-".equals(roll)) {
            this.rolls[rollIndex] = 0;
        } else {
            this.rolls[rollIndex] = Integer.valueOf(roll);
        }
        rollIndex++;
    }

    boolean isFull() {
        return rollIndex > 1 || isStrike();
    }

    boolean isStrike() {
        return rolls[0] == 10;
    }

    boolean isSpare() {
        return getScore() == 10 && rolls[0] != 10;
    }

    int getFirstRoll() {
        return rolls[0];
    }
}