package com.kata.tdd.game.bowling.controllers;

import com.kata.tdd.game.bowling.BowlingGame;
import com.kata.tdd.game.bowling.model.BowlingGameRequest;
import com.kata.tdd.game.bowling.model.Score;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/game")
class GameController {
    @RequestMapping(path = "/bowling/score", method = POST, consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    Score getScore(@RequestBody BowlingGameRequest gameRequest) {
            BowlingGame bowlingGame = new BowlingGame();
            for(String roll : gameRequest.getRolls()) {
                bowlingGame.addRoll(roll);
            }
            return new Score(bowlingGame.getTotalScore());
    }
}
