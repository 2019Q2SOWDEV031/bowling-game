package com.kata.tdd.game.bowling.model;

import java.util.List;

public class BowlingGameRequest {
    private List<String> rolls;

    public void setRolls(List<String> rolls) {
        this.rolls = rolls;
    }

    public List<String> getRolls() {
        return rolls;
    }
}
