package com.kata.tdd.game.bowling.model;

public class Score {
    private final String score;

    public Score(String totalScore) {
        this.score = totalScore;
    }

    public String getScore() {
        return this.score;
    }
}
