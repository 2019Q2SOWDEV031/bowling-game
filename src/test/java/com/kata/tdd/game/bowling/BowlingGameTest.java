package com.kata.tdd.game.bowling;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class BowlingGameTest {

    private BowlingGame bowlingGame;

    @Before
    public void init() {
        bowlingGame = new BowlingGame();
    }

    @Test
    public void totalScoreShouldBe0WhenGameBegins() {
        assertThat(bowlingGame.getTotalScore(), is("0"));
    }

    @Test
    public void totalScoreShouldBe5ForARollOf5() {
        bowlingGame.addRoll("5");
        assertThat(bowlingGame.getTotalScore(), is("5"));
    }

    @Test
    public void totalScoreShouldBe8ForTwoRollsOf5And3() {
        bowlingGame.addRoll("5");
        bowlingGame.addRoll("3");
        assertThat(bowlingGame.getTotalScore(), is("8"));
    }

    @Test
    public void totalScoreShouldBe18ForTwoFramesOfScore9And9() {
        bowlingGame.addRoll("4");
        bowlingGame.addRoll("5");
        bowlingGame.addRoll("3");
        bowlingGame.addRoll("6");
        assertThat(bowlingGame.getTotalScore(), is("18"));
    }

    @Test
    public void totalScoreShouldBe22ForASpareFollowedByRollsOf3And6() {
        bowlingGame.addRoll("4");
        bowlingGame.addRoll("/");
        bowlingGame.addRoll("3");
        bowlingGame.addRoll("6");
        assertThat(bowlingGame.getTotalScore(), is("22"));
    }

    @Test
    public void totalScoreShouldBe28ForAStrikeFollowedByRollsOf3And6() {
        bowlingGame.addRoll("X");
        bowlingGame.addRoll("3");
        bowlingGame.addRoll("6");
        assertThat(bowlingGame.getTotalScore(), is("28"));
    }

    @Test
    public void totalScoreShouldBe51ForTwoConsecutiveStrikesFollowedByRollsOf3And6() {
        bowlingGame.addRoll("X");
        bowlingGame.addRoll("X");
        bowlingGame.addRoll("3");
        bowlingGame.addRoll("6");
        assertThat(bowlingGame.getTotalScore(), is("51"));
    }

    @Test
    public void totalScoreShouldBe124ForAGivenRandomRollsThatIncludesStrikeAndSpare() {
        List<String> rolls = asList("X", "X", "3", "6", "1", "/", "4", "3", "2", "6", "2", "3",
                "1", "4", "6", "1", "7", "2");
        playAGameOf(rolls);
        assertThat(bowlingGame.getTotalScore(), is("106"));
    }

    @Test
    public void totalScoreShouldBe109ForAGivenRandomRollsThatHasLastFrameAsStrike() {
        List<String> rolls = asList("X", "X", "3", "6", "1", "/", "4", "3", "2", "6", "2", "3",
                "1", "4", "6", "1", "X", "1", "1");
        playAGameOf(rolls);
        assertThat(bowlingGame.getTotalScore(), is("109"));
    }

    @Test
    public void totalScoreShouldBe108ForAGivenRandomRollsThatHasLastFrameAsSpare() {
        List<String> rolls = asList("X", "X", "3", "6", "1", "/", "4", "3", "2", "6", "2", "3",
                "1", "4", "6", "1", "1", "/", "1");
        playAGameOf(rolls);
        assertThat(bowlingGame.getTotalScore(), is("108"));
    }

    private void playAGameOf(List<String> rolls) {
        for(String roll : rolls) {
            bowlingGame.addRoll(roll);
        }
    }

}
