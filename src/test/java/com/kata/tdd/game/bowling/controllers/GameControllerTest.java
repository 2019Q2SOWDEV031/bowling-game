package com.kata.tdd.game.bowling.controllers;

import com.kata.tdd.game.bowling.model.BowlingGameRequest;
import com.kata.tdd.game.bowling.model.Score;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class GameControllerTest {
    @Test
    public void scoreShouldBe8ForRollsOf5And3() {
        GameController gameController = new GameController();
        BowlingGameRequest gameRequest = new BowlingGameRequest();
        List<String> rolls = new ArrayList<>();
        rolls.add("5");
        rolls.add("3");
        gameRequest.setRolls(rolls);
        Score responseScore = gameController.getScore(gameRequest);
        assertThat(responseScore.getScore(), is("8"));
    }
}
